package iruela.referee.BD;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import iruela.referee.conectBD.ConexionBD;
import iruela.referee.obj.Equipo;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class PartidoBD extends JDialog implements ActionListener, MouseListener{

	private final JPanel contentPanel = new JPanel();
	private JTextField txtNmPartido;
	private JTextField txtEquipo_1;
	private JTextField txtEquipo;
	private JTextField txtFecha;
	private JTextField txtCategoria;
	private JTextField textLugar;
	private JTextField txtPartido;
	private JTextField txtDesplazamiento;
	ConexionBD conexion;
	Connection con = null;
	Statement sta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			PartidoBD dialog = new PartidoBD();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public PartidoBD() {
		//para que salga mensaje de confirmacion al cerrar aplicacion
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				tancaFinestra();
			}
		});
		
		setTitle("Referee Accounting");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			txtNmPartido = new JTextField();
			txtNmPartido.setText("núm partido");
			txtNmPartido.setHorizontalAlignment(SwingConstants.CENTER);
			txtNmPartido.setBounds(43, 141, 130, 26);
			contentPanel.add(txtNmPartido);
			txtNmPartido.setColumns(10);
			txtNmPartido.addMouseListener(this);
		}
		{
			txtEquipo = new JTextField();
			txtEquipo.setText("Equipo1");
			txtEquipo.setToolTipText("");
			txtEquipo.setBounds(43, 43, 131, 26);
			txtEquipo.setHorizontalAlignment(SwingConstants.CENTER);
			contentPanel.add(txtEquipo);
			txtEquipo.setColumns(10);
			txtEquipo.addMouseListener(this);
		}
		{
			txtFecha = new JTextField();
			txtFecha.setText("(yyyy-mm-dd)");
			txtFecha.setHorizontalAlignment(SwingConstants.CENTER);
			txtFecha.setBounds(261, 92, 130, 26);
			contentPanel.add(txtFecha);
			txtFecha.setColumns(10);
			txtFecha.addMouseListener(this);
		}
		{
			txtEquipo_1 = new JTextField();
			txtEquipo_1.setHorizontalAlignment(SwingConstants.CENTER);
			txtEquipo_1.setText("Equipo2");
			txtEquipo_1.setBounds(261, 43, 130, 26);
			contentPanel.add(txtEquipo_1);
			txtEquipo_1.setColumns(10);
			txtEquipo_1.addMouseListener(this);
		}
		
		txtCategoria = new JTextField();
		txtCategoria.setText("Categoria");
		txtCategoria.setHorizontalAlignment(SwingConstants.CENTER);
		txtCategoria.setColumns(10);
		txtCategoria.setBounds(261, 141, 130, 26);
		contentPanel.add(txtCategoria);
		txtCategoria.addMouseListener(this);
		
		textLugar = new JTextField();
		textLugar.setText("Lugar");
		textLugar.setHorizontalAlignment(SwingConstants.CENTER);
		textLugar.setColumns(10);
		textLugar.setBounds(43, 92, 130, 26);
		contentPanel.add(textLugar);
		textLugar.addMouseListener(this);
		
		txtPartido = new JTextField();
		txtPartido.setText("Partido €");
		txtPartido.setHorizontalAlignment(SwingConstants.CENTER);
		txtPartido.setColumns(10);
		txtPartido.setBounds(44, 191, 130, 26);
		contentPanel.add(txtPartido);
		txtPartido.addMouseListener(this);
		
		
		txtDesplazamiento = new JTextField();
		txtDesplazamiento.setText("Desplazamiento €");
		txtDesplazamiento.setHorizontalAlignment(SwingConstants.CENTER);
		txtDesplazamiento.setColumns(10);
		txtDesplazamiento.setBounds(261, 191, 130, 26);
		contentPanel.add(txtDesplazamiento);
		txtDesplazamiento.addMouseListener(this);
		
		JLabel lblInsertarDatosDel = new JLabel("Insertar datos del partido");
		lblInsertarDatosDel.setHorizontalAlignment(SwingConstants.CENTER);
		lblInsertarDatosDel.setBounds(79, 6, 283, 27);
		contentPanel.add(lblInsertarDatosDel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(this);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(this);
			}
		}
	}

	//Mensaje de confiramcion para cerrar ventana
	public void tancaFinestra(){
		Object [] opciones ={"Aceptar","Cancelar"};
		int eleccion = JOptionPane.showOptionDialog(this,"Esta seguro que quiere cerrar la aplicación?","Mensaje de Confirmación",
		JOptionPane.YES_NO_OPTION,
		JOptionPane.QUESTION_MESSAGE,null,opciones,"Aceptar");
		if (eleccion == JOptionPane.YES_OPTION){
			System.exit(0);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton) {
			JButton instance = (JButton) e.getSource();
			if(instance.getText().equals("OK")) {
				//Añadir datos a la BD
				
				//Datos para la conexion
				conexion = new ConexionBD("paises.sytes.net", "3306", "Accounting", "ausias");
				//conectamos
				
				//creamos statment
				try {
					sta = conexion.conectar(con, "ausias").createStatement();
				} catch (SQLException e1) {
					
					e1.printStackTrace();
				}
		
				//Añadimos equipos
				Equipo equipo1 = new Equipo(txtEquipo.getText());
				Equipo equipo2 = new Equipo(txtEquipo_1.getText());
				
				try {
					//insert a la BD
					sta.executeUpdate("INSERT INTO partido VALUES("+txtNmPartido.getText()+", '"
				+equipo1.getNombre()+"', '"+equipo2.getNombre()+"','"+txtCategoria.getText()+"', "+txtPartido.getText()+","
							+txtDesplazamiento.getText()+",'"+textLugar.getText()+"','"+txtFecha.getText()+"');");
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
				
			}else if(instance.getText().equals("Cancel")) {
				//cerrar ventana
				
				//cerrar conexion de la BD
				conexion.desconectar(con);
			}
		}
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getSource() instanceof JTextField) {
			JTextField instance = (JTextField) e.getSource();
			if(!instance.getText().equals("")) {
				instance.setText("");
			}
		}
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
