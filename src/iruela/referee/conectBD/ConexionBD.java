package iruela.referee.conectBD;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;


public class ConexionBD {
	
	private String host;
	private String puerto;
	private String BD;
	private String pasword;
	private String urlCon;
	
	public ConexionBD(String host, String puerto, String BD, String pasword) {
		this.host = host;
		this.puerto = puerto;
		this.BD = BD;
		this.pasword = pasword;
	}
	
	public Connection conectar(Connection con, String usuario) {
		urlCon = "jdbc:mysql://"+host+":"+puerto+"/"+BD;
			try {
				con = DriverManager.getConnection(urlCon, usuario, pasword);
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			return con;	
	}
	
	public void desconectar(Connection con) {
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
