package iruela.referee.interficie;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import iruela.referee.BD.PartidoBD;
import iruela.referee.conectBD.ConexionBD;

import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.Box;
import java.awt.Choice;

public class Menu extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JComboBox comboBoxMes;
	Connection con = null;
	Statement sta;
	//Para hacer una select
	ResultSet res;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public Menu() {
	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnPartidos = new JMenu("Partidos");
		menuBar.add(mnPartidos);
		
		JMenuItem mntmAadir = new JMenuItem("Añadir");
		mnPartidos.add(mntmAadir);
		mntmAadir.addActionListener(this);
		
		JMenuItem mntmQuitar = new JMenuItem("Quitar");
		mnPartidos.add(mntmQuitar);
		mntmQuitar.addActionListener(this);
		
		JMenuItem mntmVerPorMes = new JMenuItem("Ver por mes");
		menuBar.add(mntmVerPorMes);
		mntmVerPorMes.addActionListener(this);
		
		JMenuItem mntmAyuda = new JMenuItem("Ayuda");
		menuBar.add(mntmAyuda);
		
		Component horizontalStrut = Box.createHorizontalStrut(214);
		menuBar.add(horizontalStrut);
		mntmAyuda.addActionListener(this);
		
		//panel
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JMenuItem) {
			JMenuItem instance = (JMenuItem) e.getSource();
			if(instance.getText().equals("Añadir")) {
				//Abrir JDialog para insertar datos a la BD
				PartidoBD datos = new PartidoBD();
				datos.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				datos.setVisible(true);
				
			}else if(instance.getText().equals("Quitar")) {
				
			}else if(instance.getText().equals("Ver por mes")) {
				
				//desplegable mensual
				comboBoxMes = new JComboBox();
				//añadir variables al desplegable
				comboBoxMes.addItem("Enero");
				comboBoxMes.addItem("Febrero");
				comboBoxMes.addItem("Marzo");
				comboBoxMes.addItem("Abril");
				comboBoxMes.addItem("Mayo");
				comboBoxMes.addItem("Junio");
				comboBoxMes.addItem("Julio");
				comboBoxMes.addItem("Agosto");
				comboBoxMes.addItem("Septiembre");
				comboBoxMes.addItem("Octubre");
				comboBoxMes.addItem("Noviembre");
				comboBoxMes.addItem("Diciembre");
				contentPane.add(comboBoxMes, BorderLayout.NORTH);
				setContentPane(contentPane);
				
				ConexionBD conexion = new ConexionBD("paises.sytes.net", "3306", "Accounting", "ausias");
				conexion.conectar(con, "ausias");
				try {
					sta = conexion.conectar(con, "ausias").createStatement();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
				if(e.getSource() instanceof JComboBox) {
					JComboBox instanceCombo = (JComboBox) e.getSource();
					if(instanceCombo.getSelectedItem().equals("Enero")) {
						try {
							ResultSet res = sta.executeQuery("SELECT * FROM partido WHERE MONTH(fecha) = 1 ");
							int i = 0;
							while (res.next()) {
								int numPartido = res.getInt(1);
								String equipo1 = res.getString(2);
								String equipo2 = res.getString(3);
								String categoria = res.getString(4);
								double liqPartido = res.getFloat(5);
								double liqDespl = res.getFloat(6);
								String lugar = res.getString(7);
								String dia = res.getString(8);
								i++;
								//JLabels (mostrar resultados)
								System.out.println("Partido "+i+": \n");
								System.out.println("-------------------\n");
								System.out.println();
							}
							
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		}
		
	}

}
