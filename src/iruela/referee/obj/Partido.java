package iruela.referee.obj;

public class Partido {

	private Equipo equipo;
	private String categoria;
	private String fecha;
	private int numPartido;
	private double costePartido;
	private double coseteDespl;
	
	public Partido(Equipo equipo, String categoria, String fecha, int numPartido, double costePartido, double costeDespl) {

		this.equipo = equipo;
		this.categoria = categoria;
		this.fecha = fecha;
		this.numPartido = numPartido;
		this.costePartido = costePartido;
		this.coseteDespl = costeDespl;
	}

	public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getNumPartido() {
		return numPartido;
	}

	public void setNumPartido(int numPartido) {
		this.numPartido = numPartido;
	}

	public double getCostePartido() {
		return costePartido;
	}

	public void setCostePartido(double costePartido) {
		this.costePartido = costePartido;
	}

	public double getCoseteDespl() {
		return coseteDespl;
	}

	public void setCoseteDespl(double coseteDespl) {
		this.coseteDespl = coseteDespl;
	}

	@Override
	public String toString() {
		return "Partido [equipo=" + equipo.getNombre() + ", categoria=" + categoria + ", fecha=" + fecha + ", numPartido="
				+ numPartido + ", costePartido=" + costePartido + ", coseteDespl=" + coseteDespl + "]";
	}
	
	
}
